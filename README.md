# Why this fork

When launching the container with a start policy such as `--restart=always`, the restart will fail because Unicorn's pid already exists
